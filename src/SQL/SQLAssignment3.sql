DELIMITER $$
create procedure UpdateTable(IN v_action varchar(1),
							IN v_prod_id varchar(6),
							IN v_prod_name varchar(10),
							IN v_prod_desc varchar(20),
							IN v_price decimal(8,2))
BEGIN

IF(v_action='I')THEN
insert into product(prod_id,prod_name,price,prod_desc)
values(v_prod_id,v_prod_name,v_price,v_prod_desc);

ELSEIF (v_action='D') THEN
DELETE FROM product where prod_id=v_prod_id;

ELSE
UPDATE product SET prod_name=v_prod_name, prod_desc=v_prod_desc, price=v_price 
WHERE prod_id=prod_id;

END IF;
commit;
END$$
DELIMITER ;

call UpdateTable('U','EL102','LAPTOP','DELL',500);

commit;

select * from product;
