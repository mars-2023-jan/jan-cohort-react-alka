import "./App.css";
// import Home from "./redux/Home";
import Home from "./components/crud/Home";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Aside from "./components/Aside";
import Reducer from "./components/Reducer"
import MainSection from "./components/MainSection";
import LoginRoutes from "./components/Login/LoginRoutes";
// import MainComp from "./MainComp";
// import Master from "./Master";
import {configureStore} from '@reduxjs/toolkit'
import counterReducer from "./redux/counterReducer";
import { Provider } from "react-redux";
import myLogger from "./redux/myLogger";
import capAtTen from "./redux/capAtTen";
import colorReducer from "./redux/colorReducer";
import { rootReducer } from "./redux/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import ProductHome from "./components/ProductApp/ProductHome";

const reduxDevTools = composeWithDevTools();
function App() {
  console.log('Creating the store....')
  const store = configureStore({reducer:rootReducer,middleware:[myLogger,capAtTen,thunk],devTools:reduxDevTools})
  // const store = configureStore({reducer:rootReducer})
  return (
    // <div className="App">
    <Provider store={store}>
      <Home/>
       <ProductHome/>
      </Provider>
      // {/* <Reducer/> */}
      // {/* <LoginRoutes/> */}
      // {/* <MainComp/> */}
      // {/* <Master/> */}
      // {/* <Header/>
      // <div>
      // <Aside/>
      // <MainSection/>
      // </div>
      // <Footer/> */}
      // {/* <div className="CenterAlign">
      // <Home/>
      // </div> */}
    // </div>
  );
}

export default App;
