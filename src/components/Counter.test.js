import React from "react";
import {fireEvent, render,screen} from '@testing-library/react'
import userEvent from "@testing-library/user-event";
import Counter from "./counter"

describe ('Testing counter component',()=>{
    test('counter is incremented on add button click',()=>{
        render(<Counter/>)
        
        const counter = screen.getByTestId('counter')
        // const incrementBtn = screen.getByText('Add');

        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))

        expect(counter.innerHTML).toBe("3")
    });

    test('counter is incremented on add button click',()=>{
        render(<Counter/>)
        const counter = screen.getByTestId('counter')
        // const decrementBtn = screen.getByText('Subtract');

        fireEvent.click(screen.getByText('Subtract'))
        fireEvent.click(screen.getByText('Subtract'))

        expect(counter.innerHTML).toBe("-2")
    });

})