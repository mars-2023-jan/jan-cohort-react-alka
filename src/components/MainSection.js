import React from 'react';

function MainSection(props) {
    return (
        <div class="mainColor" id="main">
            <h2>This is the main content</h2>
        </div>
    );
}

export default MainSection;