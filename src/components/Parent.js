import React, { useState } from 'react';
import DynamicComponent from './DynamicComponent';
import {useNavigate} from 'react-router-dom'

function Parent(props) {
    const[user,setUser] = useState("usera")
    const navigate = useNavigate();
    function handleCLick(){
        // navigate('/footer')
        // navigate(-1) 
        // navigate(1)
        navigate('/')
    }
    return (
        <div>
            <DynamicComponent user={user}/>
            <button onClick={()=> setUser("usera")}>Switch to User A</button><br/>
            <button onClick={()=> setUser("userb")}>Switch to User B</button><br/>
            <button onClick={handleCLick}>Go Home</button><br/>
        </div>
    );
}

export default Parent;