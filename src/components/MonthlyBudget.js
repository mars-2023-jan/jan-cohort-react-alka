import { useState } from 'react';

function MonthlyBudget() {


  const [balance, setBalance] = useState({
    totalBudget: "",
    remBalance: "",
    childCare: "",
    insurance: "",
    mortgage: "",
    grocery: ""
  });

  const updateChange = (e) => {
    setBalance({ ...balance, [e.target.name]: e.target.value });
  };

  return (
    <form >
      <h1>Monthly Budget Details</h1>
      <table>
        <tr>
          <td>
            Total Budget : <input type="number" name="totalBudget" value={balance.totalBudget} onChange={updateChange} />
          </td>
          <td>
            Monthly Balance : <input type="number" name="remBalance" value={(balance.totalBudget) - (balance.childCare) - (balance.insurance) - (balance.mortgage) - (balance.grocery)} /><br></br>
          </td>
        </tr>
        <tr>
          Child Care: <input type="number" name='childCare' value={balance.childCare} onChange={updateChange} /><br></br>
        </tr>
        <tr>
          Insurance : <input type="number" name='insurance' value={balance.insurance} onChange={updateChange} /><br></br>
        </tr>
        <tr>
          Mortgage : <input type='number' name='mortgage' value={balance.mortgage} onChange={updateChange} /><br></br>
        </tr>
        <tr>
          Grocery: <input type='number' name='grocery' value={balance.grocery} onChange={updateChange} /><br></br>
        </tr>
      </table>
    </form>

  );
}
export default MonthlyBudget;
