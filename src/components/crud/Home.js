import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import AddEmployee from './AddEmployee';
import EditEmployee from './EditEmployee';
import EmpDetails from './EmpDetails';

function Home(props) {
    return (
        <div>
            <Router>
                <Routes>
                    <Route path='/' element={<EmpDetails/>}/>
                    <Route path='/edit' element={<EditEmployee/>}/>
                    <Route path='/add' element={<AddEmployee/>}/>
                </Routes>
            </Router>
        </div>
    );
}

export default Home;