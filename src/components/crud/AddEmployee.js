import React,{useEffect, useState} from 'react';
import Employees from './Employees';
import { useNavigate } from 'react-router-dom';

function AddEmployee(props) {
    let navigate = useNavigate();
    const [name,setName]= useState('')
    const [age,setAge]= useState('')
    const [newId, setNewId] = useState('')

    const handleClick =(e)=>{
        e.preventDefault();
        console.log("Map size is "+Employees.length)
        console.log("Name is" +Employees.map(emp => emp.name))
        if(Employees.length == 0)
        var newId = 1;
        else
        var newId = (Math.max(...Employees.map(emp => emp.id)))+1
        console.log("Adding employee with name :"+ name + ", age : "+age+", id : "+newId)
        const newEmp = {'id': newId,'name': name,'age':age}
        Employees.push(newEmp)
        navigate('/')
    }

    return (
        <div>
            <form>
                <input 
                type="text"
                placeholder='Enter Name'
                name = "name"
                // value = {name}
                onChange = {(e)=> setName(e.target.value)}/>
                <br/><br/>
                <input 
                type="number"
                placeholder='Enter Age'
                name="age"
                // value = {age}
                onChange = {(e)=> setAge(e.target.value)}/>
                <br/><br/>
                <button onClick={(e) => handleClick(e)} type='submit'>Add</button>
            </form>
        </div>
    );
}

export default AddEmployee;