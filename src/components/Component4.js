import {userContext} from "react";
import React from 'react';
import {UserContext} from "./Component1"

const user = userContext(UserContext)
function Component4() {
    return (
        <div>
          <h2>Component 4</h2>
          <h2> Hello {user} Again!</h2>  
        </div>
    );
}

export default Component4;