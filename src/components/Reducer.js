import React,{useReducer} from 'react';
import '/node_modules/bootstrap/dist/css/bootstrap.min.css'

const ACTIONS = {
    INCREMENT : 'increment',
    DECREMENT : 'decrement',
    INPUTCHANGE : 'inputChange',
    TOGGLECOLOR : 'toggleColor'
}
const reducer=(state,action)=>{
    switch(action.type){
        case ACTIONS.INCREMENT :
            return {...state,counter:state.counter+1}
        case ACTIONS.DECREMENT :
            return {...state,counter:state.counter-1}
        case ACTIONS.INPUTCHANGE:
            return {...state,inputVal:action.payload}
        case ACTIONS.TOGGLECOLOR:
            return {...state,color:!state.color}
        case 'default' :
            return state;
    }
}

function Reducer(props) {
    // const[inputVal,setInputVal] = useState('');
    // const[counter,setCounter] = useState(0);
    // const[color,setColor] = useState(false)

    const[state,dispatch] = useReducer(reducer,{counter:0,inputVal:'',color:false})

    return (
        <div className='form-group' style={{color:state.color?'red':'black'}}>
            
            <input className='form-control'
                type="text"
                placeholder='Enter text'
                // onChange={e=>setInputVal(e.target.value)}
                onChange={(e)=>dispatch({type:ACTIONS.INPUTCHANGE,payload:e.target.value})}
                /><br/><br/>
                <p>{state.inputVal}</p>
                <p>{state.counter}</p>
                <button 
                    type='button' 
                    className='btn btn-info'
                    // onClick={e=>setCounter(counter+1)}>+</button>
                    onClick={()=>dispatch({type:ACTIONS.INCREMENT})}>+</button>
                <button 
                    type='button' 
                    className='btn btn-info'
                    // onClick={e=>setCounter(counter-1)}>-</button>
                    onClick={()=>dispatch({type:ACTIONS.DECREMENT})}>-</button>
                    <br/><br/>
                <button 
                    type='button' 
                    className='btn btn-dark'
                    // onClick={()=>setColor(!color)}>Toggle color</button>
                    onClick={()=>dispatch({type:ACTIONS.TOGGLECOLOR})}>Toggle color</button>
                
        </div>
    );
}

export default Reducer;
