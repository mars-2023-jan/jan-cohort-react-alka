import axios from "axios"

export const getProductData=()=>{
console.log("Getting product data from the api")
    return async(dispatch)=>{
        const response = await axios.get("http://localhost:8181/api/SortedProducts")
        console.log(response.data)
        dispatch({
            type : "FETCH_DATA",
            payload: response.data
        })
    }
}

export const updateProductData=(prod)=>{
    console.log("Updating the product...")
    console.log(prod.prodId)

    return async(dispatch)=>{
        const response = await axios.put("http://localhost:8181/api/product/"+prod.prodId,prod)
        console.log(response.data)
        dispatch({
            type : "FETCH_DATA",
            payload: response.data
        })
    }
}

export const addProductData=(prod)=>{
    console.log("Adding the product...")
    console.log(prod.prodId)
    console.log(prod.prodName)


    return async(dispatch)=>{
        const response = await axios.put("http://localhost:8181/api/product",prod)
        console.log(response.data)
        dispatch({
            type : "FETCH_DATA",
            payload: response.data
        })
    }
}

export const deleteProductData=(prodId)=>{

    console.log("Deleting the product : "+ prodId)
    return async(dispatch)=>{
        const response = await axios.delete("http://localhost:8181/api/product/"+prodId)
        console.log(response.data)
        dispatch({
            type : "FETCH_DATA",
            payload: response.data
        })
    }
}
