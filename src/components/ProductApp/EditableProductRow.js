import React from 'react';
import { FaSave } from 'react-icons/fa';

const EditableProductRow = ({editProdData,handleEditChange}) => {
    console.log("Inside editable product row")
    return (
        <tr>
            <td>
                <input 
                    type="text"
                    required="required"
                    name="prodName"
                    value={editProdData.prodName}
                    placeholder='Enter Product Name'
                    onChange={handleEditChange}
                />
            </td>
            <td>
                <input 
                    type="text"
                    required="required"
                    name="prodDesc"
                    value={editProdData.prodDesc}
                    placeholder='Enter Product Description'
                    onChange={handleEditChange}
                />
            </td>
            <td>
                <input 
                    type="text"
                    required="required"
                    name="price"
                    value={editProdData.price}
                    placeholder='Enter Price'
                    onChange={handleEditChange}
                />
            </td>
            <td>
                <button type="submit">
                    <FaSave/>
                </button>
            </td>
        </tr>
    );
};

export default EditableProductRow;