import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import EditProduct from './EditProduct';
import AddProduct from './AddProduct';
import ProductDetails from './ProductDetails';

function ProductHome(props) {
    return (
        <div>
            <Router>
                <Routes>
                    <Route path='/' element={<ProductDetails/>}/>
                    <Route path='/edit' element={<EditProduct/>}/>
                    <Route path='/add' element={<AddProduct/>}/>
                </Routes>
            </Router>
        </div>
    );
}

export default ProductHome;