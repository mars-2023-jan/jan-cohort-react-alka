import React from 'react';
import { getProductData,updateProductData,addProductData,deleteProductData } from './ProductAction';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useState,Fragment } from 'react';
import ReadOnlyProductRow from './ReadOnlyProductRow';
import EditableProductRow from './EditableProductRow';
import {nanoid} from 'nanoid';

function ProductDetails() {

  console.log('State is getting accessed')
  const state = useSelector(state => state)
  console.log(state)
  const dispatch = useDispatch();
  console.log("Table Data" + state.tabData.prodId)
  const tableData = state.tabData
  console.log("Table size is :" + Object.keys(tableData).length)
    
  const [addFormData,setAddFormData] = useState({
    prodName:'',
    prodDesc:'',
    price:''
})

const [editProdData,setEditProdData] = useState({
  prodName:'',
  prodDesc:'',
  price:''
});

const [editProdId,setEditProdId]=useState(null)

  const handleEditClick = (e,prod) =>{
    console.log("Inside handlEditClick ..Updating the product : "+prod.prodId)

    e.preventDefault();
    setEditProdId(prod.prodId)
    const formValues ={
        prodName:prod.prodName,
        prodDesc:prod.prodDesc,
        price:prod.price
    }
    setEditProdData(formValues)

}

const handleEditChange =(e) => {
  console.log("Inside handleEditChange ")
    e.preventDefault();
    const fieldName = e.target.getAttribute('name');
    const fieldValue = e.target.value

    const newFormData = {...editProdData}
    newFormData[fieldName] = fieldValue

    setEditProdData(newFormData)
}

const handleEditProdSubmit = async (e)=>{
  console.log("inside handleEditProdSubmit to update the product "+editProdId) 
  e.preventDefault();
  const editedProd = {
      prodId:editProdId,
      prodName : editProdData.prodName,
      prodDesc: editProdData.prodDesc,
      price : editProdData.price
  };
  dispatch(updateProductData(editedProd));
  await sleep(2000);
  dispatch(getProductData())
}
const handleAddFormChange= (e)=>{
  e.preventDefault();
  const fieldName = e.target.getAttribute('name')
  const fieldValue = e.target.value

  const newFormData = {...addFormData}
  newFormData[fieldName] = fieldValue
  setAddFormData(newFormData)

  console.log(addFormData)
}
const sleep = ms => new Promise(
  resolve => setTimeout(resolve, ms)
);

const handleAddFormSubmit = async (e)=>{
  e.preventDefault();
  console.log("ADDING PRODUCT...")
  const newProd = {
      prodId : 'EL201',
      prodName : addFormData.prodName,
      prodDesc : addFormData.prodDesc,
      price    : addFormData.price
  }
  dispatch(addProductData(newProd));
  await sleep(1000);
    dispatch(getProductData())
}
const  handleDeleteClick = async (e, prod) => {
  e.preventDefault();
  console.log("Product to be deleted is"+prod.prodId)
  dispatch(deleteProductData(prod.prodId));
  await sleep(1000);
  dispatch(getProductData())

}
    return (
        <div className='App'>
      <button onClick={() => dispatch(getProductData())}>Get Products</button>

      <form onSubmit={handleEditProdSubmit} >
            <table >
                <thead>
                    <tr>
                        <th>Prod Name</th>
                        <th>Prod Desc</th>
                        <th>Price</th>
                        <th colSpan={2}>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        tableData.length >= 0
                        ?
                        tableData.map((prod)=>{
                            return(
                                <Fragment>
                                    {editProdId === prod.prodId ? (
                                    <EditableProductRow editProdData={editProdData}
                                    handleEditChange = {handleEditChange}/>
                                    )
                                    :(
                                    <ReadOnlyProductRow 
                                    prod={prod}
                                    handleEditClick = {handleEditClick}
                                     handleDeleteClick = {handleDeleteClick}
                                    />
                                    )}
                                </Fragment>
                            )
                        })
                        :
                        'No Data Found'
                    }
                </tbody>
            </table>
            </form>
            <br/>
            <h2>Add Product</h2>
            <form onSubmit={handleAddFormSubmit}>
                <input
                    type="text"
                    name="prodName"
                    required = "required"
                    placeholder='Enter Product Name'
                    onChange={handleAddFormChange}
                    />
                 <input
                    type="text"
                    name="prodDesc"
                    required = "required"
                    placeholder='Enter Product Description'
                    onChange={handleAddFormChange}
                    />
                     <input
                    type="text"
                    name="price"
                    required = "required"
                    placeholder='Enter Price'
                    onChange={handleAddFormChange}
                    />
                    <button type="submit">Add Product</button> 
             </form>


      
     
      </div>
  );
}
export default ProductDetails;