import React from 'react';
import {FaEdit} from 'react-icons/fa'
import {RiDeleteBinLine} from 'react-icons/ri'

const ReadOnlyProductRow = ({prod,handleEditClick,handleDeleteClick}) => {
    console.log("inside ReadOnlyProductRow")

    return (
            <tr key={prod.ProdId}>
                <td>{prod.prodName}</td>
                <td>{prod.prodDesc}</td>
                <td>{prod.price}</td>
                <td>
                    <button 
                    type="button"
                    onClick={(p)=>handleEditClick(p,prod)}>
                    <FaEdit/>
                    </button>
                </td>
                <td>
                    <button type="button"
                    onClick={(p)=> handleDeleteClick(p, prod)}>
                        <RiDeleteBinLine />
                    </button>
                </td>

            </tr>
    );
};

export default ReadOnlyProductRow;