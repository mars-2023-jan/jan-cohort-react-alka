import React, {useState } from 'react';

// function Hobbies(props) {
//     // State with list of all checked item
//     const [checked, setChecked] = useState([]);
//     const checkList = ["Sports", "Music", "Travel", "Reading"];
  
//     // Add/Remove checked item from list
//     const handleCheck = (event) => {
//       var updatedList = [...checked];
//       if (event.target.checked) {
//         updatedList = [...checked, event.target.value];
//       } else {
//         updatedList.splice(checked.indexOf(event.target.value), 1);
//       }
//       setChecked(updatedList);
//     };
  
//     // Generate string of checked items
//     const checkedItems = checked.length
//       ? checked.reduce((total, item) => {
//           return total + ", " + item;
//         })
//       : "";
  
//     // Return classes based on whether item is checked
//     var isChecked = (item) =>
//       checked.includes(item) ? "checked-item" : "not-checked-item";
  
//     return (
//       <div className="app">
//         <div className="checkList">
//           <div className="list-container">
//             {checkList.map((item, index) => (
//               <div key={index}>
//                 <input value={item} type="checkbox" onChange={handleCheck} />
//                 <span className={isChecked(item)}>{item}</span>
//               </div>
//             ))}
//           </div>
//         </div>
  
//         <div>
//           {`My Hobbies are: ${checkedItems}`}
//         </div>
//       </div>
//     );
//   }
//   export default Hobbies;

  function Hobbies(props){
    const[hobbies, setHobbies] = useState([])

    const handleChange = (e) =>{
        if(e.target.checked)
            setHobbies(arr => [...arr,e.target.value])
        else
            setHobbies(arr => arr.filter(val => val!==e.target.value))
    }

    return(
        <div>
            <input
            type="checkbox"
            value="Sports"
            onChange={handleChange} />Sports
            <br/>
            <input
            type="checkbox"
            value="Music"
            onChange={handleChange} />Music
            <br/>
            <input
            type="checkbox"
            value="Travel"
            onChange={handleChange} />Travel
            <br/>
            <input
            type="checkbox"
            value="Reading"
            onChange={handleChange} />Reading
            <br/>
            HOBBIES : {hobbies.map(hobbie => '['+ hobbie + ']')}
        </div>
    )
  }

  export default Hobbies;
