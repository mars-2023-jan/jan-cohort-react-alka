import { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
function Hooks() {
  const [count, setCount] = useState(0)
  const [calculation, setCalculation] = useState(0)

  function handleClick() {
    navigate('/')
  }
  useEffect(() => {
    setCalculation(() => count * 2)
  }, [count])

  const navigate = useNavigate()
  function handleClick(){
    navigate('/')
  }


  return (
    <div>
      <h1>Count:{count}</h1>
      <button onClick={() => setCount(count + 1)}>Add</button>
      <input type="text" name="cc" onChange={e => console.log(e.target.value)}></input>
      <p>Calculation : {calculation}</p>
      <button onClick={handleClick}>Add</button>
      <Button>BS Button</Button>
    </div>

  )
}
export default Hooks;
