import React from 'react';

function Demo(props) {
    return (
        <div>
            <h1>Hello World</h1>
            <p data-testId="testId">This is a testing paragraph</p>
        
        <div>
            <ul>
                <li>Count</li>
                <li>Color</li>
                <li>testing</li>
                <li>test</li>

            </ul>
        </div>
        </div>
    );
}

export default Demo;