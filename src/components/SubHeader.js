import React from "react";
class SubHeader extends React.Component{
    constructor(){
        super();
        this.state = {
            name:'Peter',
            id: 101
        }
    }
    render(){
        return(
            <div>
            <h3>This is a SubHeader written as a class component</h3>
            <p>Scope: {this.props.scope}</p>
            <p>name in subheader is:{this.state.name}</p>
            </div>

        )
        
    }
}

export default SubHeader;