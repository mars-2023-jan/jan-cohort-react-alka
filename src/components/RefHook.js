import React, { useRef,useEffect,useState } from 'react';

function RefHook(props) {
    const [inputValue,setInputValue] =useState('')
    const [isInitial, setIsInitial] = useState(true)
    // const count = useRef(0);

    const previousInputValue = useRef('')
    

    useEffect(()=>{
        // count.current = count.current + 1
        // if(isInitial){
        //     setIsInitial(false)
        // }
        // console.log(count.current)
        // if(!isInitial)
        // count.current = count.current+1;
        previousInputValue.current = inputValue;
    })

    return (
        <div>
            <input
            type="text"
            value = {inputValue}
            onChange = {(e) => setInputValue(e.target.value)} />
            {/* <h3>Render Count : {count.current}</h3> */}
            <h3>current Value : {inputValue}</h3>
            <h3>Previous Value : {previousInputValue.current}</h3>
        </div>
    );
}

export default RefHook;