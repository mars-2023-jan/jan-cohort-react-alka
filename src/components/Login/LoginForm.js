import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";

function LoginForm(props) {
    const navigate = useNavigate()

    const [inputValues, setInputValue] = useState({
        username: "",
        email: "",
        password: "",
    });

    //handle submit updates
    function handleChange(e) {
        const { name, value } = e.target;
        setInputValue({ ...inputValues, [name]: value });
    }

    const handleSubmit = (e) => {
        if ((inputValues.username === "Alka") && (inputValues.email === "AlkaA@marsreturnship.com") && (inputValues.password === "pwd456")) {
            navigate("/loginsuccess");
        } else {
            navigate("/loginfail")
        }

    };

    return (

            <div class="container">
                <label for="username"><b>Username : </b></label>
                <input type="text" placeholder="Username" name="username" onChange={(e) => handleChange(e)} value={inputValues.username} required /><br />
                <label for="password"><b>Password : </b></label>
                <input type="password" placeholder="Password" name="password" onChange={(e) => handleChange(e)} value={inputValues.password} required /><br />
                <label for="email"><b>Email : </b></label>
                <input type="email" placeholder="Email" name="email" onChange={(e) => handleChange(e)} value={inputValues.email} required /><br />

                <button type="submit" onClick={handleSubmit} >Sign In</button>
            </div>
    );
}

export default LoginForm;



