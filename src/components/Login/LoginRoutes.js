import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginForm from './LoginForm';
import LoginFail from './LoginFail';
import LoginSuccess from './LoginSuccess';

function LoginRoutes(props) {
    return (
        <div>
            <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<LoginForm />} />
                        <Route path="/loginform" element={<LoginForm />} />
                        <Route path="/loginsuccess" element={<LoginSuccess />} />
                        <Route path="/loginfail" element={<LoginFail />} />
                    </Routes>
            </BrowserRouter>

        </div>
    );
}

export default LoginRoutes;