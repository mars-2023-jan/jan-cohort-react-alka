import React from 'react';

function LoginFail(props) {
    return (
        <div>
            <h3>Login Failed! Please try again!</h3>
        </div>
    );
}

export default LoginFail;