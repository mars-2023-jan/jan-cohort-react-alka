import React from 'react';
import { useSelector } from 'react-redux';
import { increment, decrement } from './actions';
import { useDispatch } from 'react-redux';
import { getData } from './actions';

function Home(props) {
  console.log('State is getting accessed')
  const state = useSelector(state => state)
  console.log(state)
  const dispatch = useDispatch();
  console.log("Table Data" + state.tabData)
  const tableData = state.tabData.filter((item, index) => ((index + 1) % 2) === 0)
  console.log("Table size is :" + tableData.length)

  return (
    <div className='App'>
      <h2>counter:{state.count.counter}</h2>
      <button onClick={() => dispatch(increment())}>Add</button>
      <button onClick={() => dispatch(decrement())}>Subtract</button>
      <hr />
      <input
        type="radio"
        name="color"
        value="Red"
        onClick={(e) => dispatch({ type: 'CHANGECOLOR', payload: e.target.value })} />RED &nbsp;
      <input
        type="radio"
        name="color"
        value="Blue"
        onClick={(e) => dispatch({ type: 'CHANGECOLOR', payload: e.target.value })} />BLUE &nbsp;
      <input
        type="radio"
        name="color"
        value="Green"
        onClick={(e) => dispatch({ type: 'CHANGECOLOR', payload: e.target.value })} />GREEN &nbsp;
      <p>My favourite color is {state.favColor.color}</p>
      <hr />
      <button onClick={() => dispatch(getData())}>GetData</button>
      {tableData.length > 0 ?
        <table>
          <tr>
            <th>UserId</th>
            <th>Id</th>
            <th>Title</th>
            <th>Body</th>
          </tr>
          {tableData.map((item, index) => (
            <tr key={index}>
              <td>{item.userId}</td>
              <td>{item.id}</td>
              <td>{item.title}</td>
              <td>{item.body}</td>
            </tr>
          ))}
        </table> : <br />}
    </div>
  );
}

export default Home;