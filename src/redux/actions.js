import axios from "axios"
import { useState } from "react"

export const increment = ()=>{
    console.log("Add Action called")
    return {
        type: 'INCREMENT'
    }
}

export const decrement = ()=>{
    console.log("Subtract Action called")
    return {
        type: 'DECREMENT'
    }
}
export const getData=()=>{

    return async(dispatch)=>{
        const response = await axios.get("http://3.92.229.207:8181/api/SortedProducts")
        // const response = await axios.get("https://jsonplaceholder.typicode.com/posts")

        console.log(response.data)
        dispatch({
            type : "FETCH_DATA",
            payload: response.data
        })
    }
}