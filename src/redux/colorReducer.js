const intitialState={
    counter:0
}

const colorReducer=(state=intitialState,action)=>{
    const newState={...state};
    switch(action.type){
    case 'CHANGECOLOR':
        newState.color = action.payload;
        return newState;
    default :
        return newState
}
}

export default colorReducer;